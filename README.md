# a-particle-sym

Demo deployed via GitLab Pages to: https://marpyre.gitlab.io/a-particle-sym/

Credit to font awesome for icons.
License: https://creativecommons.org/licenses/by/4.0/
Changes made to size and colors of tint and snowflake svgs to create pngs.
Play and pause svgs used as is.

## Tech

UI:

* React *via Create React App
* Redux
* Proptypes
* Font Awesome
* Bootstrap
* HTML canvas

Testing:

* Jest *via Create React App
* Enzyme

* Other tech provided via Create React App, but not customized.

## Prerequisites:

* Node.js

Tested running in Centos7. (Should be cross-platform).

## Run Locally:

if yarn available:

```
yarn
yarn start
```

otherwise, npm:

```
npm install
npm start
```

Go to http://localhost:3000

## Tests

if yarn available:

```
yarn test
```

otherwise, npm:

```
npm test
```

Note: It's interactive... hit `a` to run all tests, `q` to quit.

## Production Build

if yarn available:

```
yarn build
```

otherwise, npm:

```
npm build
```

This creates a `build` directory with static files for serving.
