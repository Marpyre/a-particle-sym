import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    playFunc: jest.fn(),
    pauseFunc: jest.fn(),
    play: false,
  }

  const enzymeWrapper = shallow(<App {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

it('renders without crashing', () => {
  const { enzymeWrapper } = setup();
  expect(enzymeWrapper.find('div').hasClass('App')).toBe(true);
  expect(enzymeWrapper.find('header').hasClass('App-header')).toBe(true);
  expect(enzymeWrapper.find('Canvas').exists());
});
