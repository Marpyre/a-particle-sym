import reducer from './sim'
import { sim_control as controlActions, sim_configure as configureActions } from '../actions';
import { types } from '../actions/sim-configure.js';

const initialState = {play: false, simType: types.SNOW, speed: 3, wind: 3};

describe('sim reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  })

  it('should handle PLAY', () => {
    expect(
      reducer(initialState, {
        type: controlActions.PLAY,
      })
    ).toEqual({play: true, simType: types.SNOW, speed: 3, wind: 3})
  })

  it('should handle PAUSE', () => {
    expect(
      reducer(initialState, {
        type: controlActions.PAUSE,
      })
    ).toEqual({play: false, simType: types.SNOW, speed: 3, wind: 3})
  })

  it('should handle TYPE_SET SNOW', () => {
    expect(
      reducer(initialState, {
        type: configureActions.TYPE_SET,
        simType: types.SNOW,
      })
    ).toEqual({play: false, simType: types.SNOW, speed: 3, wind: 3})
  })

  it('should handle TYPE_SET RAIN', () => {
    expect(
      reducer(initialState, {
        type: configureActions.TYPE_SET,
        simType: types.RAIN,
      })
    ).toEqual({play: false, simType: types.RAIN, speed: 7, wind: 0})
  })
})
