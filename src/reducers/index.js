import { combineReducers } from 'redux';
import sim from './sim';

export default combineReducers({
    sim
})