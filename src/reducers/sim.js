import { sim_control as controlActions, sim_configure as configureActions } from '../actions';
import { types } from '../actions/sim-configure.js';

const sim = (state = {play: false, simType: types.SNOW, speed: 3, wind: 3}, action) => {
    switch (action.type) {
        case controlActions.PLAY:
            return { ...state, play: true };
        case controlActions.PAUSE:
            return { ...state, play: false };
        case configureActions.TYPE_SET:
            if(action.simType === types.SNOW) return { ...state, simType: action.simType, speed: 3, wind: 3 }
            else return { ...state, simType: action.simType, speed: 7, wind: 0 }
        default:
            return state;
    }
}

export default sim;