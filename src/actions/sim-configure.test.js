import { sim_configure as actions } from './index.js';
import { types, setType } from './sim-configure.js';

describe('sim-configure actions', () => {
  it('should create an action to set type', () => {
    const simType = types.SNOW;
    const expectedAction = {
      type: actions.TYPE_SET,
      simType
    }
    expect(setType(types.SNOW)).toEqual(expectedAction)
  })
})
