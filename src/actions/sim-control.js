import { sim_control as actions } from './index.js';

/**
 * Action Creators
 */

export function play() {
  return {
    type: actions.PLAY
  }
}

export function pause() {
  return {
    type: actions.PAUSE
  }
}
