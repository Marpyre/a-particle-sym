import { sim_control as actions } from './index.js';
import { play, pause } from './sim-control.js';

describe('sim-configure actions', () => {
  it('should create an action to play', () => {
    const expectedAction = {
      type: actions.PLAY
    }
    expect(play()).toEqual(expectedAction)
  })
  
  it('should create an action to pause', () => {
    const expectedAction = {
      type: actions.PAUSE
    }
    expect(pause()).toEqual(expectedAction)
  })
})
