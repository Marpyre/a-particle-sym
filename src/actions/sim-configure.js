import { sim_configure as actions } from './index.js';

/**
 * Constants
 */

export const types = {
  SNOW : 'SNOW',
  RAIN: 'RAIN',
}

/**
 * Action Creators
 */

export function setType(simType) {
  return {
    type: actions.TYPE_SET,
    simType
  }
}
