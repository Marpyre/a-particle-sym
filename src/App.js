import React from 'react';
import CanvasContainer from './components/canvas/CanvasContainer.js';
import ControlsContainer from './components/controls/ControlsContainer.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>A Particle Sim</h1>
      </header>
      <ControlsContainer/>
      <CanvasContainer/>
    </div>
  );
}

export default App;
