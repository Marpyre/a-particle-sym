import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Controls from './Controls';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    playFunc: jest.fn(),
    pauseFunc: jest.fn(),
    play: false,
    setTypeFunc: jest.fn(),
  }

  const enzymeWrapper = shallow(<Controls {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

it('renders without crashing', () => {
  const { enzymeWrapper } = setup();
  expect(enzymeWrapper.find('div').hasClass('Controls')).toBe(true);
  expect(enzymeWrapper.find('div').children()).toHaveLength(4);
});
