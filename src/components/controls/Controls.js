import React from 'react';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { types } from '../../actions/sim-configure.js';
import './Controls.css';

function Controls(props) {
  const {play, playFunc, pauseFunc, setTypeFunc, simType} = props;

  return (
    <div className="Controls">
      <Button variant="outline-primary" className={'mr-2'} active={simType === types.SNOW} onClick={() => {setTypeFunc(types.SNOW)}} size={'sm'}>
        Snow 
      </Button>
      <Button variant="outline-primary" className={'mr-5'} active={simType === types.RAIN} onClick={() => {setTypeFunc(types.RAIN)}} size={'sm'}>
        Rain
      </Button>
      <Button variant="outline-primary" className={'mr-2'} active={play} onClick={playFunc} size={'sm'}>
        <FontAwesomeIcon icon={faPlay} /> 
      </Button>
      <Button variant="outline-primary" active={!play} onClick={pauseFunc} size={'sm'}>
        <FontAwesomeIcon icon={faPause} /> 
      </Button>
    </div>
  );
}

Controls.propTypes = {
  play: PropTypes.bool.isRequired,
  playFunc: PropTypes.func.isRequired,
  pauseFunc: PropTypes.func.isRequired,
  setTypeFunc: PropTypes.func.isRequired,
};

export default Controls;
