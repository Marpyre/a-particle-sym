import { connect } from 'react-redux'
import Controls from './Controls.js';
import { play, pause } from '../../actions/sim-control.js';
import { setType } from '../../actions/sim-configure.js';

const mapStateToProps = (state) => {
  return {
    play: state.sim.play,
    simType: state.sim.simType,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    playFunc: () => {
      dispatch(play())
    },
    pauseFunc: () => {
      dispatch(pause())
    },
    setTypeFunc: (type) => {
      dispatch(setType(type))
    },
  }
}

const ControlsContainer = connect( mapStateToProps, mapDispatchToProps)(Controls);

export default ControlsContainer;
