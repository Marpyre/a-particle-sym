import { connect } from 'react-redux'
import Canvas from './Canvas.js';

const mapStateToProps = (state) => {
  return {
    play: state.sim.play,
    simType: state.sim.simType,
    speed: state.sim.speed,
    height: 500,
    width: 500,
    wind: state.sim.wind,
  }
}

const CanvasContainer = connect( mapStateToProps)(Canvas);

export default CanvasContainer;
