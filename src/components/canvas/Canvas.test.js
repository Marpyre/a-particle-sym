import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Canvas from './Canvas';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    height: 500,
    play: false,
    simType: '',
    speed: 5,
    width: 500,
    wind: 5,
  }

  const enzymeWrapper = shallow(<Canvas {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

it('renders without crashing', () => {
  const { enzymeWrapper } = setup();
  expect(enzymeWrapper.find('div').hasClass('Canvas')).toBe(true);
});
