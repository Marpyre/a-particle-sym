import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { types } from '../../actions/sim-configure.js';
import './Canvas.css';

import snowPng from './snow.png';
import rainPng from './rain.png';

const image = new Image();

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

//https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations
class Canvas extends React.Component {

  constructor(props) {
    super(props);
    this.draw = this.draw.bind(this);
  }

  items = [];

  componentDidMount() {
    if(this.props.simType === types.SNOW) image.src = snowPng;
    else image.src = rainPng;
    this.triggerAnimation(this.props.play);
  }

  triggerAnimation(play) {
    window.requestAnimationFrame(this.draw);
  }

  componentDidUpdate(prevProps){
    if(this.props.play !== prevProps.play){
      this.triggerAnimation(this.props.play);
    }

    if(this.props.simType !== prevProps.simType){
      // update images
      if(this.props.simType === types.SNOW) image.src = snowPng;
      else image.src = rainPng;
      
      // to clear previous sim
      // this.items = [];
    }
  }

  componentWillUnmount() {
    window.cancelAnimationFrame(this.draw);
  }

  draw(){
    let items = this.items;
    const {height, play, speed, width, wind} = this.props;

    if(play){
      var ctx = document.getElementById('canvas').getContext('2d');

      ctx.globalCompositeOperation = 'destination-over';
      ctx.clearRect(0, 0, width, height); // clear canvas
    
      // add an item along top randomly
      items.push(
        {
          x: getRandomInt(0, width-20), 
          y: 0,
        }
      );

      // update items to animate falling and drifting
      _.forEach(items, (item)  => {
        ctx.drawImage(image, item.x, item.y);
        item.y += speed;
        item.x += wind;
        if(item.x > width) item.x = width-item.x;
      });

      // don't track items after they leave the canvas
      items = _.filter(items, (item) => {
        return item.y < height-20;
      })    

      window.requestAnimationFrame(this.draw); // draw updates
    }
  }

  render() { 
    const {height, width} = this.props;

    return (
      <div className="Canvas">
        <canvas id="canvas" width={width} height={height}> </canvas>
      </div>
    );
  }
}

Canvas.propTypes = {
  height: PropTypes.number.isRequired,
  play: PropTypes.bool.isRequired,
  simType: PropTypes.string.isRequired,
  speed: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  wind: PropTypes.number.isRequired,
};

export default Canvas;
